using UnityEngine;
using Tamaloo108.GSM.Core.SO;
using Tamaloo108.GSM.Core;

namespace Tamaloo108.GSM.Sample.Conditions
{
    [CreateAssetMenu(fileName = "Move", menuName = "STVR/GSM/Conditions/Move Conditions")]
    public class MoveConditionSO : ConditionSO<MoveCondition> { }

    public class MoveCondition : Condition
    {
        float _vertical;
        float _horizontal;

        public override bool Statement()
        {
            _horizontal = Input.GetAxisRaw("Horizontal");
            _vertical = Input.GetAxisRaw("Vertical");

            StateMachine.DataTransfer.Position = new Vector3(_horizontal, 0f, _vertical);

            return _horizontal != 0 || _vertical != 0;
        }
    }
}