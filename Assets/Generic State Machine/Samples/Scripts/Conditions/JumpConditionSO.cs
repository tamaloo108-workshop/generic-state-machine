﻿using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Conditions
{
    [CreateAssetMenu(fileName = "Jump", menuName = "STVR/GSM/Conditions/Jump Conditions")]
    public class JumpConditionSO : ConditionSO<JumpCondition> { }

    public class JumpCondition : Condition
    {
        bool _isJump;

        public override bool Statement()
        {
            _isJump = Input.GetKeyDown(KeyCode.Space);
            return _isJump;
        }
    }
}