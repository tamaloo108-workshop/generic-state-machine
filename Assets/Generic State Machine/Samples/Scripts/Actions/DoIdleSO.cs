﻿using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Actions
{
    [CreateAssetMenu(fileName = "Do Idle", menuName = "STVR/GSM/Actions/Do Idle")]
    public class DoIdleSO : ActionSO<DoIdle>
    {

    }


    public class DoIdle : Action
    {
        public override void OnEnter()
        {
            Debug.Log("enter idle");
        }

        public override void OnExit()
        {
            Debug.Log("exit idle");
        }

        public override void OnUpdate() { }
    }

}