﻿using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Actions
{
    [CreateAssetMenu(fileName = "Do Rotate", menuName = "STVR/GSM/Actions/Do Rotate")]
    public class DoRotateSO : ActionSO<DoRotate>
    {
        [Range(1f, 100f)] public float RotateSpeed = 10f;
    }

    public class DoRotate : Action
    {
        private CharacterController _cc;
        private DoRotateSO _doRotateSO;
        private Quaternion _targetRotation;

        protected override void GetRequestComponent(StateMachine stateMachine)
        {
            base.GetRequestComponent(stateMachine);
            _cc = stateMachine.GetComponent<CharacterController>();
            _doRotateSO = (DoRotateSO)_actionSO;
        }

        public override void OnUpdate()
        {
            if (_stateMachine.DataTransfer.Position == Vector3.zero) return;
            _targetRotation = Quaternion.LookRotation(_stateMachine.DataTransfer.Position);
            _cc.transform.rotation = Quaternion.Slerp(_cc.transform.rotation, _targetRotation, _doRotateSO.RotateSpeed * Time.deltaTime);
        }

        public override void OnEnter() { }

        public override void OnExit() { }
    }

}