using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Actions
{
    [CreateAssetMenu(fileName = "Do Move", menuName = "STVR/GSM/Actions/Do Move")]
    public class DoMoveSO : ActionSO<DoMove>
    {
        [Range(1f, 125f)] public float MoveSpeed = 10f;
    }

    public class DoMove : Action
    {
        private CharacterController _cc;
        private DoMoveSO _doMoveSO;

        protected override void GetRequestComponent(StateMachine stateMachine)
        {
            base.GetRequestComponent(stateMachine);
            _cc = stateMachine.GetComponent<CharacterController>();
            _doMoveSO = (DoMoveSO)_actionSO;
        }

        public override void OnUpdate()
        {
            _stateMachine.DataTransfer.Position.Normalize();

            _cc.Move(_stateMachine.DataTransfer.Position * _doMoveSO.MoveSpeed * Time.deltaTime);
        }

        public override void OnEnter()
        {
            Debug.Log("move enter");
        }

        public override void OnExit()
        {
            Debug.Log("move exit");
        }
    }
}