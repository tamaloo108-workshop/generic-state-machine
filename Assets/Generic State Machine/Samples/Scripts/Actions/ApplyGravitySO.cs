﻿using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Actions
{
    [CreateAssetMenu(fileName = "ApplyGravity", menuName = "STVR/GSM/Actions/Apply Gravity")]
    public class ApplyGravitySO : ActionSO<ApplyGravity>
    {
        [Range(-10f, 10f)] public float GravityMultiplier = 2f;
    }

    public class ApplyGravity : Action
    {
        private CharacterController _cc;
        ApplyGravitySO _applyGravitySO;

        protected override void GetRequestComponent(StateMachine stateMachine)
        {
            base.GetRequestComponent(stateMachine);
            _cc = stateMachine.GetComponent<CharacterController>();
            _applyGravitySO = (ApplyGravitySO)_actionSO;
        }

        public override void OnUpdate()
        {
            _stateMachine.DataTransfer.GravityValue = Physics.gravity.y * _applyGravitySO.GravityMultiplier;

            if (_cc.isGrounded && _stateMachine.DataTransfer.Velocity.y < 0f)
                _stateMachine.DataTransfer.Velocity.y = -2f;

            _stateMachine.DataTransfer.Velocity.y += _stateMachine.DataTransfer.GravityValue * Time.deltaTime;
            _cc.Move(_stateMachine.DataTransfer.Velocity * Time.deltaTime);
        }

        public override void OnEnter() { }

        public override void OnExit() { }
    }
}