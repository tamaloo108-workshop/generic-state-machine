﻿using Tamaloo108.GSM.Core;
using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Sample.Actions
{
    [CreateAssetMenu(fileName = "Do Jump", menuName = "STVR/GSM/Actions/Do Jump")]
    public class DoJumpSO : ActionSO<DoJump>
    {
        public float JumpHeight = 2f;
    }

    public class DoJump : Action
    {
        private CharacterController _cc;
        private DoJumpSO _actionJump;

        protected override void GetRequestComponent(StateMachine stateMachine)
        {
            base.GetRequestComponent(stateMachine);
            _cc = stateMachine.GetComponent<CharacterController>();
            _actionJump = (DoJumpSO)_actionSO;
        }

        public override void OnEnter()
        {
            Debug.Log("jump enter");
        }

        public override void OnExit()
        {
            Debug.Log("jump exit");
        }

        public override void OnUpdate()
        {
            if (_cc.isGrounded)
                _stateMachine.DataTransfer.Velocity.y += Mathf.Sqrt(_actionJump.JumpHeight * -3.0f * _stateMachine.DataTransfer.GravityValue);

            _cc.Move(_stateMachine.DataTransfer.Velocity * Time.deltaTime);
        }
    }
}