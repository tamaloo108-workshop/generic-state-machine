﻿namespace Tamaloo108.GSM.Core.SO
{
    public partial class TransitionTableSO
    {
        [System.Serializable]
        public struct TransitionFormat
        {
            public StateSO From;
            public StateSO To;
            public ConditionStatement[] ConditionStatements;

            public TransitionFormat(StateSO from, StateSO to, ConditionStatement[] conditionStatements)
            {
                From = from;
                To = to;
                ConditionStatements = conditionStatements;
            }
        }
    }
}
