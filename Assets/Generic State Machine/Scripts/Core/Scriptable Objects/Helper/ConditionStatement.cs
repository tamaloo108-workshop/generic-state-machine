﻿
namespace Tamaloo108.GSM.Core.SO
{
    public partial class TransitionTableSO
    {
        [System.Serializable]
        public struct ConditionStatement
        {
            public ConditionSO Condition;
            public StatementResultEnum ExpectedResult;

            public ConditionStatement(ConditionSO condition, StatementResultEnum expectedResult)
            {
                Condition = condition;
                ExpectedResult = expectedResult;
            }
        }
    }
}
