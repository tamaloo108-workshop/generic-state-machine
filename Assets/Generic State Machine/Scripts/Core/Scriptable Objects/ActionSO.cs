﻿using UnityEngine;

namespace Tamaloo108.GSM.Core.SO
{
    public abstract class ActionSO : ScriptableObject
    {
        private Action _action;

        public Action GetAction(StateMachine stateMachine)
        {
            if (_action == null)
            {
                _action = CreateAction();
                _action.Init(this, stateMachine);
            }

            return _action;
        }

        protected abstract Action CreateAction();
    }

    public abstract class ActionSO<T> : ActionSO where T : Action, new()
    {
        protected override Action CreateAction() => new T();
    }
}