﻿using UnityEngine;

namespace Tamaloo108.GSM.Core.SO
{
    [CreateAssetMenu(fileName = "New Transition", menuName = "STVR/GSM/Transition/Create new Transition")]
    public partial class TransitionTableSO : ScriptableObject
    {
        [SerializeField] private TransitionFormat[] TransitionsList;
        private Transition _transition;
        private State _lastState;

        public void Initialize(StateMachine stateMachine, State initialState)
        {
            _transition = new Transition(stateMachine, TransitionsList);
            _lastState = initialState;
        }

        public bool TryUpdateState(out State targetState)
        {
            targetState = null;
            for (int i = 0; i < TransitionsList.Length; i++)
                if (_transition.TryValidateTransition(_lastState.StateSO, TransitionsList[i].To, out targetState))
                {
                    if (targetState != _lastState)
                    {
                        _lastState = targetState;
                        return true;
                    }
                }

            return false;
        }
    }
}