﻿using System;
using UnityEngine;

namespace Tamaloo108.GSM.Core.SO
{
    [CreateAssetMenu(fileName = "new State", menuName = "STVR/GSM/State/Create new State")]
    public class StateSO : ScriptableObject
    {
        [SerializeField] private string StateName;
        [SerializeField] private ActionSO[] _actions;

        State _state;

        internal State GetState(StateMachine stateMachine)
        {
            if (_state == null)
            {
                _state = new State(stateMachine, StateName, this, _actions);
            }

            return _state;
        }
    }
}