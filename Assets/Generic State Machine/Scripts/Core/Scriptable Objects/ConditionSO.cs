using UnityEngine;

namespace Tamaloo108.GSM.Core.SO
{
    public abstract class ConditionSO : ScriptableObject
    {
        private Condition _condition;

        public Condition GetCondition(StateMachine stateMachine)
        {
            if (_condition == null)
            {
                _condition = CreateCondition();
                _condition.Init(this, stateMachine);
            }

            return _condition;
        }

        protected abstract Condition CreateCondition();
    }

    public abstract class ConditionSO<T> : ConditionSO where T : Condition, new()
    {
        protected override Condition CreateCondition() => new T();
    }

}