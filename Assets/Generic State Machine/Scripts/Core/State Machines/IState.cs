﻿namespace Tamaloo108.GSM.Core
{
    public interface IState
    {
        void OnEnter();
        void OnExit();
        void OnUpdate();
    }
}