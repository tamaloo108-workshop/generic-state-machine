﻿using Tamaloo108.GSM.Core.SO;

namespace Tamaloo108.GSM.Core
{
    public abstract class Action : IState
    {
        protected ActionSO _actionSO;
        protected StateMachine _stateMachine;

        public void Init(ActionSO actionSO, StateMachine stateMachine)
        {
            _actionSO = actionSO;
            GetRequestComponent(stateMachine);
        }

        /// <summary>
        /// Executed on enter state.
        /// </summary>
        public abstract void OnEnter();

        /// <summary>
        /// Executed on exit state
        /// </summary>
        public abstract void OnExit();

        /// <summary>
        /// Executed while on this state.
        /// </summary>
        public abstract void OnUpdate();

        /// <summary>
        /// Override this if needed to take extra component from character.
        /// On default, component you can take are that in same gameObject in stateMachine, you can extend this as you see fit.
        /// (e.g., controller from state machine, take collider from state machine, idk something like that.)
        /// </summary>
        /// <param name="stateMachine">no need do fancy stuff, ignore it since it already handled by its parent</param>
        protected virtual void GetRequestComponent(StateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }
    }
}