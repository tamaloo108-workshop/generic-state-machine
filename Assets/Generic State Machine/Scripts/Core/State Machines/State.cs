﻿using System;
using Tamaloo108.GSM.Core.SO;

namespace Tamaloo108.GSM.Core
{

    public class State : IState
    {
        public string StateName { get; }
        public ActionSO[] ActionSO { get; }
        public StateSO StateSO { get; }        
        private StateMachine _stateMachine;

        public State(StateMachine stateMachine, string stateName, StateSO stateSO, ActionSO[] _actions)
        {
            StateName = stateName;
            ActionSO = _actions;
            _stateMachine = stateMachine;
            StateSO = stateSO;
        }

        public void OnUpdate()
        {
            for (int i = 0; i < ActionSO.Length; i++)
            {
                ActionSO[i].GetAction(_stateMachine).OnUpdate();
            }
        }

        public void OnEnter()
        {
            for (int i = 0; i < ActionSO.Length; i++)
            {
                ActionSO[i].GetAction(_stateMachine).OnEnter();
            }
        }

        public void OnExit()
        {
            for (int i = 0; i < ActionSO.Length; i++)
            {
                ActionSO[i].GetAction(_stateMachine).OnExit();
            }
        }
    }
}