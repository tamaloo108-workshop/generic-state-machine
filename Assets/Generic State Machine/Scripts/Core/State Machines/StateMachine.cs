﻿using Tamaloo108.GSM.Core.SO;
using UnityEngine;

namespace Tamaloo108.GSM.Core
{
    public class StateMachine : MonoBehaviour
    {
        [SerializeField] private StateSO _initialState;
        [SerializeField] private TransitionTableSO _transition;

        [SerializeField] private bool _handleDataTransfer;

        private State _currentState;
        private State _targetState;

        private BaseDataTransfer _dataTransfer;
        public BaseDataTransfer DataTransfer => _dataTransfer;

        private void Awake()
        {
            if (_handleDataTransfer)
                _dataTransfer = new BaseDataTransfer();
        }

        private void Start()
        {
            _currentState = _initialState.GetState(this);
            _transition.Initialize(this, _currentState);
        }

        private void Update()
        {
            if (_transition.TryUpdateState(out _targetState))
                DoTransition(_targetState);

            _currentState.OnUpdate();
        }

        private void DoTransition(State targetState)
        {
            _currentState.OnExit();
            _currentState = targetState;
            _currentState.OnEnter();
        }
    }
}