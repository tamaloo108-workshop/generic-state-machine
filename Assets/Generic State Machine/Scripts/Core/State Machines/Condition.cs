﻿using Tamaloo108.GSM.Core.SO;

namespace Tamaloo108.GSM.Core
{
    public abstract class Condition
    {
        protected ConditionSO ConditionSO;
        protected StateMachine StateMachine;

        public virtual void Init(ConditionSO conditionSO, StateMachine stateMachine)
        {
            ConditionSO = conditionSO;
            GetRequiredComponent(stateMachine);
        }

        /// <summary>
        /// Fill this with statement (or chunk of code, maybe.) as prerequisite for do transition.
        /// </summary>
        /// <returns>True if statement is fulfilled, otherwise false.</returns>
        public abstract bool Statement();

        /// <summary>
        /// Override this if needed to take extra component from character.
        /// On default, component you can take are that in same gameObject in stateMachine, you can extend this as you see fit.
        /// (e.g., controller from state machine, take collider from state machine, idk something like that.)
        /// </summary>
        /// <param name="stateMachine">no need do fancy stuff, ignore it since it already handled by its parent</param>
        protected virtual void GetRequiredComponent(StateMachine stateMachine)
        {
            StateMachine = stateMachine;
        }
    }
}