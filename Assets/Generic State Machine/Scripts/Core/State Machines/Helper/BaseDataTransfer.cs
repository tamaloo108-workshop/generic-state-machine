using UnityEngine;

namespace Tamaloo108.GSM.Core
{
    /// <summary>
    /// Helper to transfer data between state.
    /// you can use your own custom data transfer too.
    /// or just extends from this.
    /// </summary>
    public class BaseDataTransfer
    {
        public float GravityValue = 0f;
        public Vector3 Position = Vector3.zero;
        public Vector3 Velocity = Vector3.zero;
    }
}