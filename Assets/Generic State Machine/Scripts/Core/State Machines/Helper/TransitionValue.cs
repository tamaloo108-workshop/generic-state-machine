﻿using static Tamaloo108.GSM.Core.SO.TransitionTableSO;

namespace Tamaloo108.GSM.Core
{
    public struct TransitionValue
    {
        public Condition Condition;
        public StatementResultEnum ExpectResult;

        public TransitionValue(Condition condition, StatementResultEnum expectResult)
        {
            Condition = condition;
            ExpectResult = expectResult;
        }
    }
}
