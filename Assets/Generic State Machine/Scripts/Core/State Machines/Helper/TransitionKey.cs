﻿using Tamaloo108.GSM.Core.SO;

namespace Tamaloo108.GSM.Core
{
    public struct TransitionKey
    {
        public StateSO From;
        public StateSO To;

        public TransitionKey(StateSO from, StateSO to)
        {
            From = from;
            To = to;
        }
    }

}
