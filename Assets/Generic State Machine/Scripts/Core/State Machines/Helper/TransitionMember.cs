﻿namespace Tamaloo108.GSM.Core
{
    public struct TransitionMember
    {
        public TransitionKey Key;
        public TransitionValue[] Value;

        public TransitionMember(TransitionKey key, TransitionValue[] value)
        {
            Key = key;
            Value = value;
        }
    }
}
