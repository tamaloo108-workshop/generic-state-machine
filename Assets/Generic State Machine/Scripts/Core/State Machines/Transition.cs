using Tamaloo108.GSM.Core.SO;
using static Tamaloo108.GSM.Core.SO.TransitionTableSO;

namespace Tamaloo108.GSM.Core
{
    public class Transition
    {
        private readonly TransitionMember[] _transitionMember;
        private int _conditionResultCount = 0;
        private int _tempIndex = 0;
        private StateMachine _stateMachine;
        private Condition _tempCondition;
        private StatementResultEnum _tempResult;
        private TransitionKey _tempKey;
        private TransitionKey _tempFindKey;

        public Transition(StateMachine stateMachine, TransitionFormat[] transitionsList)
        {
            _stateMachine = stateMachine;
            _transitionMember = new TransitionMember[transitionsList.Length];

            for (int i = 0; i < transitionsList.Length; i++)
                AddNewTransitionMember(i, transitionsList[i].From, transitionsList[i].To, transitionsList[i].ConditionStatements);
        }

        private void AddNewTransitionMember(int idx, StateSO from, StateSO to, ConditionStatement[] conditionStatements)
        {
            _transitionMember[idx] = new TransitionMember(new TransitionKey(from, to), new TransitionValue[conditionStatements.Length]);

            for (int i = 0; i < _transitionMember[idx].Value.Length; i++)
                _transitionMember[idx].Value[i] = new TransitionValue(conditionStatements[i].Condition.GetCondition(_stateMachine), conditionStatements[i].ExpectedResult);
        }

        public bool TryValidateTransition(StateSO from, StateSO to, out State state)
        {
            _tempKey = new TransitionKey(from, to);
            state = ShouldTransition(_tempKey) ? to.GetState(_stateMachine) : from.GetState(_stateMachine);
            return ShouldTransition(_tempKey);
        }

        private bool ShouldTransition(TransitionKey tKey)
        {
            _conditionResultCount = 0;

            for (int i = 0; i < _transitionMember[GetTransitionMemberIndex(tKey)].Value.Length; i++)
            {
                _tempCondition = _transitionMember[GetTransitionMemberIndex(tKey)].Value[i].Condition;
                _tempResult = _transitionMember[GetTransitionMemberIndex(tKey)].Value[i].ExpectResult;
                if (_tempCondition.Statement() == ExpectedResult(_tempResult))
                    _conditionResultCount++;
            }

            return _conditionResultCount == _transitionMember[GetTransitionMemberIndex(tKey)].Value.Length;
        }

        private bool ExpectedResult(StatementResultEnum result)
        {
            if (result == 0)
                return true;

            if ((int)result == 1)
                return false;

            return false;
        }

        private int GetTransitionMemberIndex(TransitionKey key)
        {
            void LocalGetTransitionIndex(TransitionKey _key, TransitionMember[] _transition, out int idx)
            {
                idx = 0;
                for (int i = 0; i < _transition.Length; i++)
                {
                    if (_transition[i].Key.From == _key.From && _transition[i].Key.To == _key.To)
                    {
                        idx = i;
                        break;
                    }
                }
            }

            LocalGetTransitionIndex(key, _transitionMember, out _tempIndex);
            _tempFindKey = _transitionMember[_tempIndex].Key;
            LocalGetTransitionIndex(_tempFindKey, _transitionMember, out _tempIndex);

            return _tempIndex;
        }
    }
}
